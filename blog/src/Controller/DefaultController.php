<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController{
	/**
	 *@Route("/")
	 */
	public function index(){
		$posts=[
			[
				'id'=>1,
				'title'=>'Post 1',
				'created_at'=>'2019-09-16 20:26:00'
			],
			[
				'id'=>2,
				'title'=>'Post 2',
				'created_at'=>'2019-09-16 20:26:00'
			],
			[
				'id'=>3,
				'title'=>'Post 3',
				'created_at'=>'2019-09-16 20:26:00'
			],
		];

		return  $this->render('index.html.twig',[
			'title'=>'Postagem Teste',
			'posts'=>$posts
		]);
	}
	/**
	 *@Route("/post/{slug}")
	 */
	public function single($slug){
		return $this->render('single.html.twig',
			[
				'slug'=>$slug
			]);
	}
}